package data
{
	public class Settings
	{
		public static const GAME_WIDTH:int = 1080;
		public static const GAME_HEIGHT:int = 1920;
		public static const TABLEAU_COLUMNS:int = 8;
		
		private static var _selectedDeckBack:String
		private static var _gameScore:int;
		private static var _timeBonus:int;

		

		public static function get selectedDeckBack():String
		{
			return _selectedDeckBack;
		}

		public static function set selectedDeckBack(value:String):void
		{
			_selectedDeckBack = value;
		}

		public static function get gameScore():int
		{
			return _gameScore;
		}

		public static function set gameScore(value:int):void
		{
			_gameScore = value;
		}

		public static function get timeBonus():int
		{
			return _timeBonus;
		}

		public static function set timeBonus(value:int):void
		{
			_timeBonus = value;
		}


	}
}