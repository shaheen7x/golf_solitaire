package data
{
	public class EndGameState
	{
		public static const WIN:String = "you_win";
		public static const TIME_UP:String = "time_up";
		public static const OUT_OF_MOVES:String = "no_more_moves";
	}
}