package views
{
	import assets.Assets;
	
	import events.GameEvents;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class StartView extends Sprite
	{
		private var playBtn:Button;
		private var label:TextField;
		private var backBtn:Button;
		public function StartView()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			drawScrn();
		}
		
		private function drawScrn():void
		{
			this.width = Starling.current.stage.stageWidth;
			this.height =  Starling.current.stage.stageHeight;
			trace(this, this.width, this.height);
			var background:Image = new Image(Assets.assetsManager.getTexture("background"));
			this.addChild(background);
		
			playBtn = new Button(Assets.assetsManager.getTexture("play_button"));
			playBtn.addEventListener(Event.TRIGGERED, onPlayBtnTriggered);		
			this.addChild(playBtn);
			playBtn.width = Starling.current.stage.stageWidth * 0.5;
			playBtn.height = playBtn.width * 0.3;
			playBtn.x = Starling.current.stage.stageWidth/2 - playBtn.width/2;
			playBtn.y = Starling.current.stage.stageHeight/2 + playBtn.height * 2;

		}
		
		private function onBackBtnTriggered(e:Event):void
		{
			// TODO Auto Generated method stub
			
		}
		
		private function onPlayBtnTriggered(e:Event):void
		{
			playBtn.removeEventListener(Event.TRIGGERED, onPlayBtnTriggered);
			this.dispatchEventWith(GameEvents.PLAY);
		}
		public function Destroy():void
		{
			this.visible = false;
			this.removeChildren(0,-1,true);
			this.removeFromParent(true);
		}

	}
}