package views
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Circ;
	import com.greensock.easing.Elastic;
	import com.greensock.easing.Expo;
	import com.greensock.easing.Quart;
	import com.greensock.easing.Quint;
	
	import flash.geom.Point;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.setTimeout;
	
	import assets.Assets;
	
	import data.EndGameState;
	import data.Settings;
	
	import managers.GameManager;
	
	import objects.GameHeader;
	import objects.GameOverPanel;
	import objects.PlayingCard;
	import objects.fx.EndGameFX;
	import objects.fx.Points;
	import objects.fx.PoppingImg;
	import objects.fx.StackSlider;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite3D;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.utils.Align;
	
	public class GamePlayView extends Sprite3D
	{		
		private var background:Image;
		public var cardsDeckArray:Vector.<PlayingCard>;
		public var tableauArray:Array;
		public var tableauTopCardsArray:Array;
		public var stockArray:Array;
		public var foundationArray:Array;
		public var foundationCard:PlayingCard;
		public var prevFoundationCard:PlayingCard;
		public var splitCard:PlayingCard;
		private var soundChannel:SoundChannel;
		private var soundTransform:SoundTransform
		
		private var foundation_x:Number;
		private var foundation_y:Number;
		private var split_x:Number;
		private var split_y:Number;
		private var stacksPointsArray:Array;
		public var header:GameHeader;
		private var timerNotification:TextField;
		private var isGameEnded:Boolean;;
		private var undoBtn:Button;
		private var undoArray:Array;
		public var hintButton:Button;
		private var HintIndex:int;
		private var endGameFX:EndGameFX;
		public function GamePlayView()
		{
			super();
			GameManager.gamePlayView = this;
			background = new Image(Assets.assetsManager.getTexture("background_blue2"));
			this.addChild(background);
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
//			this.rotationX = 0.3;
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			
			this.width = Starling.current.stage.stageWidth;
			this.height =  Starling.current.stage.stageHeight;
			
			header = new GameHeader;
			this.addChild(header);
			soundTransform = new SoundTransform(0.4); 
			
			timerNotification = new TextField(1080,150," 000 ");
			timerNotification.format = new TextFormat("BarlowCondensedBlackItalic",120,0xffffff,Align.CENTER,Align.CENTER);
//			timerNotification.format.italic = true;
//			timerNotification.format.bold = true;
			this.addChild(timerNotification);
			timerNotification.x = -2000;
			timerNotification.y = background.height * 0.5 + 200;
			
			undoBtn = new Button(Assets.assetsManager.getTexture("undo_button"));
			this.addChild(undoBtn);
			undoBtn.x = background.width - undoBtn.width - 20;
			undoBtn.y = background.height - undoBtn.height - 50;
			undoBtn.addEventListener(Event.TRIGGERED, onUndoBtnTriggered);
			undoArray = new Array;
			
			hintButton = new Button(Assets.assetsManager.getTexture("hint_button"));
			this.addChild(hintButton);
			hintButton.x = background.width * 0.5 - undoBtn.width * 0.5;
			hintButton.y = background.height - undoBtn.height - 50;
			hintButton.addEventListener(Event.TRIGGERED, onHintBtnTriggered);
			
			createCardsDeck();
			shuffleCardsDeck();
			draw();
		}
		
		private function createCardsDeck():void
		{
			cardsDeckArray = new Vector.<PlayingCard>;
			trace("PlayingCard.SUITS_ARRAY.length",PlayingCard.SUITS_ARRAY.length);
			trace("PlayingCard.RANKS_ARRAY.length",PlayingCard.RANKS_ARRAY.length);
			for(var i:int=0; i<PlayingCard.SUITS_ARRAY.length; i++)
			{
				for(var j:int=0; j<PlayingCard.RANKS_ARRAY.length; j++)
				{
					var card:PlayingCard = new PlayingCard(PlayingCard.SUITS_ARRAY[i],PlayingCard.RANKS_ARRAY[j]);
					trace("creating card",PlayingCard.SUITS_ARRAY[i],PlayingCard.RANKS_ARRAY[j]);
					cardsDeckArray.push(card);
				}
			}
			
			for(i=cardsDeckArray.length-1; i>=0; i--)
			{
				trace(cardsDeckArray[i].cardName)
			}
		}
		private function shuffleCardsDeck():void
		{
			var tempArray:Vector.<PlayingCard> = new Vector.<PlayingCard>;
			for(var i:int=cardsDeckArray.length-1; i>=0; i--)
			{
				var rand:int = Math.floor(Math.random() * cardsDeckArray.length);
				tempArray.push(cardsDeckArray[rand]);
				cardsDeckArray.removeAt(rand);
			}
			cardsDeckArray = tempArray;
//			trace("cardsDeckArray.length",cardsDeckArray.length);
//			for(i=cardsDeckArray.length-1; i>=0; i--)
//			{
//				trace(cardsDeckArray[i].cardName)
//			}
		}
		
		public function draw():void
		{
			header.resetTimer();
			isGameEnded = false;
			undoArray = [];
			undoBtn.enabled = false;
			hintButton.enabled = false;
			HintIndex = 0;
			
			for(var j:int=cardsDeckArray.length-1; j>=0; j--)
			{
				this.addChild(cardsDeckArray[j]);
				cardsDeckArray[j].x = background.width * 0.5 - cardsDeckArray[j].width * 0.5;
				cardsDeckArray[j].y = background.height * 0.5 + cardsDeckArray[j].height * 1.5;
			}
			var column:int = 0;
			var row:int = 0;
			var gap:Number = 8;
			var topGap:Number = cardsDeckArray[0].height * 3;
			var sideGap:Number = 0.5 * (background.width - (8*cardsDeckArray[0].width + 7*gap));
			var delay:Number = 0.06;
//			sideGap = 40;
			tableauArray = new Array;
			stockArray = new Array;
			foundationArray = new Array;
			tableauTopCardsArray = new Array;
			stacksPointsArray = new Array;
			var card_x:Number;
			var card_y:Number;
			for(var i:int=0; i< 40 ; i++)
			{

				card_x = column *(cardsDeckArray[i].width + gap) + sideGap;
				card_y = row *(cardsDeckArray[i].height * 0.6 + gap) + topGap;
				
				TweenLite.to(cardsDeckArray[i] , 0.15 ,{x:card_x, y:card_y, delay:i*delay, ease:Quint.easeIn});
				setTimeout(cardsDeckArray[i].flipCardAndAddOnTop,delay*1000*i+35);
//				this.swapChildren(cardsDeckArray[i],this.getChildAt(this.numChildren-
//				TweenLite.to(cardsDeckArray[i].card , 0.25 ,{delay:0.5 ,rotationY:MathHelper.degreesToRadians(180), ease:Quint.easeInOut});
//				cardsDeckArray[i].card.setFaceUp();
				;
				if(row == 0)
				{
					tableauArray.push(new Array);
					stacksPointsArray.push(new Point(card_x,card_y));
				}
				tableauArray[column].push(cardsDeckArray[i]);
				column++
				if(column == Settings.TABLEAU_COLUMNS)
				{
					column = 0;
					row++;					
				}				
			}
			card_y = card_y + cardsDeckArray[i].height*4;
			foundation_x = background.width *0.5 - cardsDeckArray[i].width*0.5;
			foundation_y = card_y;
			split_x = foundation_x + cardsDeckArray[i].width * 1.5;
			split_y = foundation_y;
			for(i=40; i< 51 ; i++)
			{
				stockArray.push(cardsDeckArray[i]);
				card_x = foundation_x - cardsDeckArray[i].width*0.5 -((i-35) * 25)-5;
				TweenLite.to(cardsDeckArray[i] , 0.05 ,{x:card_x, y:card_y, delay:i*delay, ease:Quint.easeIn, onComplete:playDealingSound});	
			}
			
			foundationCard = cardsDeckArray[51];
			foundationCard.playCardType = PlayingCard.FOUNDATION;
			foundationArray.push(cardsDeckArray[51]);
			TweenLite.to(cardsDeckArray[51] , 0.1 ,{x:foundation_x, y:foundation_y, delay:i*delay, ease:Quint.easeIn});
			setTimeout(cardsDeckArray[51].flipCard,delay*1000*i+250);
			
			setTimeout(updateMovableCards,delay*i+4000);
			setTimeout(startTimer,delay*i+4000);
			
		}
		
		public function updateMovableCards():void
		{
			tableauTopCardsArray = new Array;
			for(var i:int=0; i<Settings.TABLEAU_COLUMNS; i++)
			{
				if(tableauArray[i].length > 0)
				{
					var card:PlayingCard = tableauArray[i][tableauArray[i].length - 1];
					tableauTopCardsArray.push(card);	
					card.playCardType = PlayingCard.TABLEAU;
				}
			}
			trace("stockArray.length",stockArray.length)
			if(stockArray.length > 0)
			{
				stockArray[0].playCardType = PlayingCard.STOCK;
			}
		}
		public function isSolvable():Boolean
		{
			var _isSolvable:Boolean = false;
			var _isSolvableWithFoundation:Boolean = false;
			var _isSolvableWithPrevFoundation:Boolean = false;
			var _isSolvableWithSplit:Boolean = false;
			for(var i:int=0; i<tableauTopCardsArray.length; i++)
			{
				if(tableauTopCardsArray[i].value == foundationCard.value+1 
					|| tableauTopCardsArray[i].value == foundationCard.value-1 
					|| (tableauTopCardsArray[i].value == 1 && foundationCard.value == 13) 
					|| (tableauTopCardsArray[i].value == 13 && foundationCard.value == 1))
				{
					_isSolvableWithFoundation = true;
					break;
				}else{
					_isSolvableWithFoundation = false;
				}
				if(splitCard && splitCard.playCardType == PlayingCard.SPLIT)
				{
					if(tableauTopCardsArray[i].value == splitCard.value+1 
						|| tableauTopCardsArray[i].value == splitCard.value-1 
						|| (tableauTopCardsArray[i].value == 1 && splitCard.value == 13) 
						|| (tableauTopCardsArray[i].value == 13 && splitCard.value == 1))
					{
						_isSolvableWithSplit = true;
						break;
					}else{
						_isSolvableWithSplit = false;
					}
				}
				if(prevFoundationCard)
				{
					if(tableauTopCardsArray[i].value == prevFoundationCard.value+1 
						|| tableauTopCardsArray[i].value == prevFoundationCard.value-1 
						|| (tableauTopCardsArray[i].value == 1 && prevFoundationCard.value == 13) 
						|| (tableauTopCardsArray[i].value == 13 && prevFoundationCard.value == 1))
					{
						_isSolvableWithPrevFoundation = true;
						break;
					}else{
						_isSolvableWithPrevFoundation = false;
					}
				}
				if(splitCard)
				{
					if(splitCard.playCardType != PlayingCard.SPLIT)
					{
						_isSolvableWithPrevFoundation = false;
					}
				}
				
			}
			trace(_isSolvableWithFoundation, _isSolvableWithPrevFoundation , _isSolvableWithSplit);
			if(_isSolvableWithFoundation || _isSolvableWithPrevFoundation || _isSolvableWithSplit)
			{
				_isSolvable = true;
			}else{
				_isSolvable = false;
			}
			return _isSolvable;
		}
		public function checkIsMovable(card:PlayingCard):Boolean
		{
			if(card.value == foundationCard.value+1 || card.value == foundationCard.value-1 
				|| (card.value == 1 && foundationCard.value == 13) || (card.value == 13 && foundationCard.value == 1))
			{
				addPoints(new Point(card.x,card.y),50);
				moveToFoundation(card);
				return true;
			}
			return false;
		}
		public function moveToFoundation(card:PlayingCard):void
		{
			addToUndoStack(card);
			if(foundationCard)
			{
				prevFoundationCard = foundationCard;
				foundationCard.playCardType = "";
			}
			if(card.playCardType == PlayingCard.TABLEAU)
			{
				for(var i:int=0; i<Settings.TABLEAU_COLUMNS; i++)
				{
					for(var j:int=0; j<tableauArray[i].length; j++)
					{
						if(card == tableauArray[i][j])
						{
							
							tableauArray[i].removeAt(j);
							if(tableauArray[i].length == 0)
							{
								var stackFX:StackSlider = new StackSlider;
								stackFX.x = stacksPointsArray[i].x;
								stackFX.y = stacksPointsArray[i].y - 100;
								this.addChild(stackFX);
								popIamge("stack_cleared",stackFX.x + stackFX.width*0.5, stackFX.y + 100);
								setTimeout(callAddPoints,300);
								function callAddPoints():void
								{addPoints(new Point(stackFX.x,stackFX.y+100),100);}
							}
						}
					}
				}
			}
			else if(card.playCardType == PlayingCard.STOCK)
			{
				stockArray.removeAt(0);
				setTimeout(card.flipCardAndAddOnTop,100);
			}
			else if(card.playCardType == PlayingCard.SPLIT)
			{
				card.playCardType = PlayingCard.FOUNDATION;
			}
			
			
//			foundationCard.playCardType = PlayingCard.FOUNDATION;
			if(splitCard)
			{
				card.playCardType = (splitCard.playCardType == PlayingCard.SPLIT) ? "" : PlayingCard.FOUNDATION;
			}else
			{
				card.playCardType = PlayingCard.FOUNDATION;
//				prevFoundationCard = foundationCard;
//				foundationCard.playCardType = "";
			}
			foundationCard = card;
			foundationArray.push(card);
			trace("card.playCardType",card.playCardType);
			TweenLite.to(card , 0.25 ,{x:foundation_x, y:foundation_y, ease:Quint.easeIn, onComplete:playDealingSound});
			card.addOnTop();
			updateMovableCards();
			checkGameState();
		}
		
		
		public function moveToSplit(card:PlayingCard):void
		{
			if(card.playCardType == PlayingCard.FOUNDATION && foundationArray.length > 1)
			{
				addToUndoStack(card);
				foundationArray.removeAt(foundationArray.indexOf(card));
				splitCard = card;
				splitCard.playCardType = PlayingCard.SPLIT;
				if(prevFoundationCard)
				{
					foundationCard = prevFoundationCard;
					foundationCard.playCardType = "";
				}
				trace("card.playCardType",card.playCardType);
				
				TweenLite.to(card , 0.25 ,{x:split_x, y:split_y, ease:Quint.easeIn, onComplete:playDealingSound});
					
			}
		}
		private function checkGameState():void
		{
			var tableauCardsCount:int = 0;
			for(var i:int=0;i<tableauArray.length ;i++)
			{
				tableauCardsCount += tableauArray[i].length;
			}
			trace("tableauCardsCount",tableauCardsCount);
			if(tableauCardsCount == 0)
			{
				hintButton.enabled = false;
				setTimeout(function():void{endGame(EndGameState.WIN)},2000);				
				return
			}
			if(stockArray.length == 0)
			{
				var boolean:Boolean = isSolvable();
				trace("boolean",boolean)
				if(!boolean)
				{
					hintButton.enabled = false;
					setTimeout(endGame,2000);
				}
			}
	
		}
		private function addPoints(point:Point, value:int):void
		{
			var points:Points = new Points(value);
			this.addChild(points);
			points.x = point.x;
			points.y = point.y - points.height;
			points.scale = 1.5;
			points.alpha = 1;
			TweenLite.to(points,1,{x:860,y:150,alpha:0.75, scale:0.75,ease:Expo.easeOut,onComplete:function():void{points.removeFromParent(true);}})
			Settings.gameScore = Settings.gameScore + value;
		}
		private function substractPoints(value:int = -50):void
		{
			if(Settings.gameScore<=0)
				return;
			
			var points:Points = new Points(value);
			this.addChild(points);
			points.x = 850;
			points.y = 70;
			points.alpha = 0.9;
			TweenLite.to(points,0.75,{x:860,y:250,alpha:0.75, scale:0.75,ease:Expo.easeOut,onComplete:function():void{points.removeFromParent(true);}})
			Settings.gameScore = Settings.gameScore + value;
		}
		
		private function playDealingSound():void
		{
			soundChannel = Assets.assetsManager.playSound("card_flip",0,0,soundTransform);
		}
		public function endGame(state:String = EndGameState.OUT_OF_MOVES):void
		{
			trace("Game Over");
			if(!isGameEnded)
			{
				isGameEnded = true;
				header.stopTimer();
				endGameFX = new EndGameFX(state);
				this.addChild(endGameFX);
			}			
		}
		public function showGameOverScrn():void
		{
			var gameOverPanel:GameOverPanel = new GameOverPanel;
			this.addChild(gameOverPanel);
			this.removeChild(endGameFX);
		}
		private function startTimer():void
		{
			notifyTimer("30");
			header.resetTimer();
			header.startTimer();
			hintButton.enabled = true;
		}
		public function notifyTimer(time:String):void
		{
			timerNotification.x = -2000;
//			timerNotification.scale = 0.1;
			alpha:0;
			var str:String = "";
			switch(time)
			{
				case "30":
					str = "3 minutes left ";
					break;
				case "20":
					str = "2 minutes left ";
					break;
				case "10":
					str = "1 minute left ";
					break;
				case "030":
					str = "30 seconds left ";
					break;
				case "010":
					str = "10 seconds left ";
					break;
				
			}
			timerNotification.text = str;
			TweenLite.to(timerNotification,0.75,{x:0,alpha:1,ease:Back.easeInOut,onComplete:onTweenDone});
			function onTweenDone():void
			{
				TweenLite.to(timerNotification,0.75,{delay:1,x:2000,alpha:0,ease:Back.easeInOut});
			}
			
		}
		private function popIamge(value:String , _x:Number = 540, _y:Number = 300):void
		{
			var popImg:PoppingImg = new PoppingImg(value);
			this.addChild(popImg);
			popImg.pivotX = popImg.width *0.5;
			popImg.pivotY = popImg.height *0.5;
			popImg.x = _x;
			popImg.y = _y;
			popImg.scale = 0;
			TweenLite.to(popImg,1.25,{x:540, y:300, scale:1,alpha:1,ease:Elastic.easeInOut,onComplete:onTweenInDone});
			function onTweenInDone():void
			{
				TweenLite.to(popImg,1.25,{delay:1,scale:0,alpha:0,ease:Elastic.easeInOut,onComplete:onTweenOutDone});
			}
			function onTweenOutDone():void
			{
				popImg.removeFromParent(true);
			}
		}
		private function onHintBtnTriggered(e:Event):void
		{
			trace("hint");
			hintButton.enabled = false;
			getHint();
		}
		public function getHint():void
		{
			var _isSolvable:Boolean = false;
			var _isSolvableWithFoundation:Boolean = false;
			var _isSolvableWithPrevFoundation:Boolean = false;
			var _isSolvableWithSplit:Boolean = false;
			var hintsArray:Array = new Array;
			for(var i:int=0; i<tableauTopCardsArray.length; i++)
			{
				if(tableauTopCardsArray[i].value == foundationCard.value+1 
					|| tableauTopCardsArray[i].value == foundationCard.value-1 
					|| (tableauTopCardsArray[i].value == 1 && foundationCard.value == 13) 
					|| (tableauTopCardsArray[i].value == 13 && foundationCard.value == 1))
				{
					_isSolvableWithFoundation = true;	
					trace("i",i,"tableauTopCardsArray[i].value",tableauTopCardsArray[i].value);
					hintsArray.push(tableauTopCardsArray[i]);
				}else{
					hintsArray.push(0);
				}
				if(splitCard && splitCard.playCardType == PlayingCard.SPLIT)
				{
					if(tableauTopCardsArray[i].value == splitCard.value+1 
						|| tableauTopCardsArray[i].value == splitCard.value-1 
						|| (tableauTopCardsArray[i].value == 1 && splitCard.value == 13) 
						|| (tableauTopCardsArray[i].value == 13 && splitCard.value == 1))
					{
						_isSolvableWithSplit = true;
					}
				}
				if(prevFoundationCard)
				{
					if(tableauTopCardsArray[i].value == prevFoundationCard.value+1 
						|| tableauTopCardsArray[i].value == prevFoundationCard.value-1 
						|| (tableauTopCardsArray[i].value == 1 && prevFoundationCard.value == 13) 
						|| (tableauTopCardsArray[i].value == 13 && prevFoundationCard.value == 1))
					{
						_isSolvableWithPrevFoundation = true;
					}
				}
				if(splitCard)
				{
					if(splitCard.playCardType == PlayingCard.SPLIT)
					{
						_isSolvableWithPrevFoundation = false;
					}
				}
				
			}
			trace(_isSolvableWithFoundation, _isSolvableWithPrevFoundation , _isSolvableWithSplit);
			trace("hintsArray.length",hintsArray.length,"hintsArray:",hintsArray);
			if(!_isSolvableWithFoundation && !_isSolvableWithPrevFoundation && !_isSolvableWithSplit)
			{
				trace("hint is stock");
				if(stockArray.length > 0)
				{
					stockArray[0].highLightCard();
					animateHintCard(stockArray[0]);
				}
				return;
			}
			
			if(_isSolvableWithPrevFoundation)
			{
				hintsArray.push(foundationCard);
			}else{
				hintsArray.push(0);
			}
			if(_isSolvableWithSplit)
			{
				hintsArray.push(splitCard);
			}else{
				hintsArray.push(0);
			}
			HintIndex++;
			if(HintIndex >= hintsArray.length)
			{
				HintIndex = 0;
			}
			while(hintsArray[HintIndex] == 0)
			{
				trace(HintIndex, hintsArray[HintIndex]);
				HintIndex++;
				if(HintIndex >= hintsArray.length)
				{
					HintIndex = 0;
				}				
			}
			trace("HintIndex",HintIndex,"hintsArray.length",hintsArray.length,"hintsArray:",hintsArray);
			hintsArray[HintIndex].highLightCard();
			animateHintCard(hintsArray[HintIndex]);

		}
		
		private function animateHintCard(card:PlayingCard):void
		{
			var counter:int = 0;
			fadeIn();
			//			var card_y:Number = this.y;
			function fadeIn():void
			{
				if(counter == 2)
				{
//					GameManager.gamePlayView.hintButton.enabled = true;
					return;
				}
				TweenLite.to(card,0.25,{y:card.y - 20,ease:Circ.easeInOut, onComplete:fadeOut});
			}
			function fadeOut():void
			{
				counter++;
				TweenLite.to(card,0.25,{y:card.y + 20,ease:Circ.easeInOut, onComplete:fadeIn});
			}
			
		}
		private function onUndoBtnTriggered(e:Event):void
		{
			trace("undo");
//			substractPoints();
			undoBtn.enabled = false;
			var card:PlayingCard = undoArray[undoArray.length-1].card;
			card.playCardType = undoArray[undoArray.length-1].type;
			TweenLite.to(card,0.25,{x:undoArray[undoArray.length-1].x,y:undoArray[undoArray.length-1].y});
			if(card.playCardType == PlayingCard.STOCK)
			{
				card.flipCardAndAddOnTop();
			}
			if(card.playCardType == PlayingCard.TABLEAU)
			{
				card.addOnTop();
				substractPoints();
			}
			tableauArray = undoArray[undoArray.length-1].tableauArray;
			tableauTopCardsArray = undoArray[undoArray.length-1].tableauTopCardsArray;
			stockArray = undoArray[undoArray.length-1].stockArray;
			foundationArray = undoArray[undoArray.length-1].foundationArray;
			foundationCard = undoArray[undoArray.length-1].foundationCard;
			prevFoundationCard = undoArray[undoArray.length-1].prevFoundationCard;
			splitCard = undoArray[undoArray.length-1].splitCard;			
			undoArray.removeAt(undoArray.length-1);
			if(undoArray.length == 0)
			{
				undoBtn.enabled = false;
			}else{
				undoBtn.enabled = true;
			}
		}
		private function addToUndoStack(card:PlayingCard):void
		{
			var obj:Object = new Object;
			obj.card = card;
			obj.x = card.x;
			obj.y = card.y;
			obj.type = card.playCardType;
			obj.tableauArray = tableauArray.concat();
			obj.tableauTopCardsArray = tableauTopCardsArray.concat();
			obj.stockArray = stockArray.concat();
			obj.foundationArray = foundationArray.concat();
			obj.foundationCard = foundationCard;
			obj.prevFoundationCard = prevFoundationCard;
			obj.splitCard = splitCard;
			undoArray.push(obj);
			undoBtn.enabled = true;
		}
		public function reset():void
		{
			undoArray = null;
			tableauArray = null;
			tableauTopCardsArray = null;
			stockArray = null;
			foundationArray = null;
			foundationCard = null;
			prevFoundationCard = null;
			splitCard = null;
			Settings.gameScore = 0;
			header.reset();

			for(var i:int=0;i<cardsDeckArray.length ;i++)
			{
				cardsDeckArray[i].card.setFaceDown();
				this.removeChild(cardsDeckArray[i]);
			}
			
			shuffleCardsDeck();
			draw();
		}
		public function destroy():void
		{
			this.visible = false;
			this.removeChildren(0,-1,true);
			this.removeFromParent(true);
		}
		
		
	}
}