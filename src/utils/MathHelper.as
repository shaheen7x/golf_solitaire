package utils
{
	public class MathHelper
	{
		public static function degreesToRadians(degrees:Number):Number
		{
			var radians:Number;
			radians = degrees * Math.PI/180;
			return radians;
		}
		
		public static function radiansToDegrees(radians:Number):Number
		{
			var degrees:Number;
			degrees = radians * 180/Math.PI
			return degrees;
		}
	}
}