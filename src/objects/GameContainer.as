package objects
{
	import assets.Assets;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class GameContainer extends Sprite
	{
		public function GameContainer()
		{
			super();
//			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
//		}
//		
//		private function onAddedToStage(event:Event):void
//		{
			 
			var bgImg:Image = new Image(Assets.assetsManager.getTexture("grid_portrait"));
//			var bgQuad:Quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,0x01512D);
			var bgQuad:Quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,0xffffff);
			this.addChild(bgQuad);
		}
		public function Destroy():void
		{
			this.visible = false;
			this.removeChildren(0,-1,true);
			this.removeFromParent(true);
		}
	}
}