package objects
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Quint;
	
	import flash.events.TimerEvent;
	import flash.text.Font;
	import flash.text.FontStyle;
	import flash.text.FontType;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	import data.EndGameState;
	import data.Settings;
	
	import managers.GameManager;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.utils.Align;
	
	public class GameHeader extends Sprite
	{
		private var scoreLabel:TextField;
		private var scoreLabelValue:int = 0;
		private var star:Star;
		private var timerLabel:TextField;
		private var timer:Timer;
		private var timerMintuts:int;
		private var timerSeconds:int;
		public function GameHeader()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var header:Quad = new Quad(1080,150,0x000000);
			header.alpha = 0.15;
			this.addChild(header);
			
			timerLabel = new TextField(220,120,"3:00");
//			timerLabel.autoScale = true;
			timerLabel.format = new TextFormat("BarlowCondensedBlackItalic",100,0xffffff,Align.LEFT,Align.CENTER);
//			timerLabel.format.bold = true;
			this.addChild(timerLabel);
			timerLabel.x = 40;
			timerLabel.y = 15;
			
			
			scoreLabel = new TextField(220,120,"0");
			scoreLabel.autoScale = true;
			scoreLabel.format = new TextFormat("BarlowCondensedBlackItalic",100,0xffffff,Align.CENTER,Align.CENTER);
//			scoreLabel.format.bold = true;
//			scoreLabel.format.italic = true;
			
			this.addChild(scoreLabel);
			scoreLabel.x = 1080-scoreLabel.width - 40;
			scoreLabel.y = 15;
			
			star = new Star;
			this.addChild(star);
			star.x = scoreLabel.x - star.width;
			star.y = 13;
			
			timer = new Timer(1000);
			timer.addEventListener(TimerEvent.TIMER, onTimerTick);
			timerMintuts = 3;
			timerSeconds = 0;
			
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onTimerTick (e:TimerEvent):void
		{

			timerSeconds -=1;
			if(timerSeconds < 0)
			{
				timerSeconds = 59;
				timerMintuts -=1;
			}
			
			var str:String = timerMintuts.toString() + timerSeconds.toString();
//			trace("str",str);
			if(timerMintuts == 2 && timerSeconds == 0)
			{
				GameManager.gamePlayView.notifyTimer(str);
			}
			if(timerMintuts == 1 && timerSeconds == 0)
			{
				GameManager.gamePlayView.notifyTimer(str);
			}
			if(timerMintuts == 0 && timerSeconds == 30)
			{
				timerLabel.format.color = 0x0ff2222;
				GameManager.gamePlayView.notifyTimer(str);
			}
			if(timerMintuts == 0 && timerSeconds == 10)
			{
				GameManager.gamePlayView.notifyTimer(str);
			}
			if(timerMintuts == 0 && timerSeconds == 0)
			{
				stopTimer();
				GameManager.gamePlayView.endGame(EndGameState.TIME_UP);
			}

			updateTimerInput();
		}
		public function startTimer():void
		{
			timer.start();
			updateTimerInput();
		}
		public function stopTimer():void
		{
			timer.stop();
			trace("timerMintuts",timerMintuts,"timerSeconds",timerSeconds);
			var timeBonus:int = timerMintuts * 60 + timerSeconds;
			Settings.timeBonus = timeBonus * 10;
		}
		public function resetTimer():void
		{
			timerLabel.format.color = 0x0ffffff;
			timer.reset();
			timerSeconds = 0;
			timerMintuts = 3;
			updateTimerInput();
			
		}
		private function updateTimerInput():void
		{
			var ss:String = "";
			var mm:String = "";
			if(timerSeconds < 10)
			{
				ss = "0" + timerSeconds.toString();
			}
			else
			{
				ss = timerSeconds.toString();
			}
			
//			if(timerMintuts < 10)
//			{
//				mm = "0" + timerMintuts.toString();
//			}
//			else
//			{
//				mm = timerMintuts.toString();
//			}
			mm = timerMintuts.toString();
			timerLabel.text = mm+":"+ss;
		}
		
		private function onEnterFrame(e:Event):void
		{
			if(scoreLabelValue < Settings.gameScore)
			{
				scoreLabelValue+=5;
				scoreLabel.text = scoreLabelValue.toString();
				scoreLabel.format.color = 0x00cc00;
//				while(scoreLabel.text.length<6)
//				{
//					scoreLabel.text = scoreLabel.text+" ";
//				}
				
				if(scoreLabelValue % 50 ==0)
				{
					TweenLite.to(star,0.1,{scale:1.1, ease:Quint.easeInOut, onComplete:function():void{star.scale = 1}});
				}
			}
//			else if(Settings.gameScore == 0)
//			{
//				scoreLabelValue = 0;
//				scoreLabel.text = "0";
//			}
			else if(scoreLabelValue > Settings.gameScore && Settings.gameScore >= 0)
			{
				scoreLabelValue-=5;
				scoreLabel.text = scoreLabelValue.toString();
				scoreLabel.format.color = 0xdd0000;
			}
			else
			{
				scoreLabel.format.color = 0xffffff;
			}
			
		}
		public function reset():void
		{
			scoreLabelValue = 0;
			scoreLabel.text = "0";
			scoreLabel.format.color = 0xffffff;
			resetTimer();
		}
		
	}
}