package objects
{
	import assets.Assets;
	
	import data.Settings;
	
	import starling.display.Image;
	import starling.display.Sprite3D;
	import starling.events.Event;
	
	import utils.MathHelper;
	
	public class PlayingCardFace extends Sprite3D
	{
		private var faceImgSrc:String;
		private var faceImg:Image;
		private var face:Sprite3D;
		private var backImg:Image;
		private var back:Sprite3D;
		private var _isFaceUp:Boolean;
		
		public function PlayingCardFace(face:String)
		{
			faceImgSrc = face;
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			faceImg = new Image(Assets.assetsManager.getTexture(faceImgSrc));
			face = new Sprite3D;
			face.addChild(faceImg);
			this.addChild(face);
			face.pivotX = face.width / 2;
			face.pivotY = face.height / 2;			
			face.x = face.width / 2;
			face.y = face.height / 2;
			face.rotationY = MathHelper.degreesToRadians(-180);
			//			front.x = front.width;			
			face.visible = false;
			
			backImg = new Image(Assets.assetsManager.getTexture(Settings.selectedDeckBack));
			back = new Sprite3D;
			back.addChild(backImg);
			this.addChild(back);
			back.pivotX = back.width / 2;
			back.pivotY = back.height / 2;			
			back.x = back.width / 2;
			back.y = back.height / 2;
			
			_isFaceUp = false;
			
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}

		private function onEnterFrame(event:Event):void
		{
			
			if(this.rotationY > MathHelper.degreesToRadians(90))
			{
				if(isFaceUp)
				{
					face.visible = false;
					back.visible = true;
				}
				else{
					face.visible = true;
					back.visible = false;
				}				
			}
		}
		
		public function setFaceUp():void
		{
			face.rotationY = 0;
			//				front.x = front.width;
			back.rotationY = MathHelper.degreesToRadians(180);
			//				back.x = 0;
			face.visible = true;
			back.visible = false;
			_isFaceUp = true;	
		}
		public function setFaceDown():void
		{
			face.rotationY = MathHelper.degreesToRadians(180);
			//				front.x = front.width;
			back.rotationY = 0;
			//				back.x = 0;
			face.visible = false;
			back.visible = true;
			_isFaceUp = false;	
		}
		
		public function get isFaceUp():Boolean
		{
			return _isFaceUp;
		}

	}
}