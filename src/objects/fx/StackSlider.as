package objects.fx
{
	import com.greensock.TweenLite;
	
	import assets.Assets;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class StackSlider extends Sprite
	{
		public function StackSlider()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			var mask:Quad = new Quad(118,1000,0x000000);
			this.addChild(mask);
			mask.x = 0;
			mask.y = 0;
			this.mask = mask;
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var img:Image = new Image(Assets.assetsManager.getTexture("yellow_stack_slider"));
			this.addChild(img);
			img.x = 0;
			img.y = 1000;
			TweenLite.to(img,1,{y:-750, onComplete:onTweenComplete});
		}
		private function onTweenComplete():void
		{
			this.removeFromParent(true);	
		}
	}
}