package objects.fx
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Elastic;
	
	import flash.utils.setTimeout;
	
	import assets.Assets;
	
	import data.EndGameState;
	
	import managers.GameManager;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class EndGameFX extends Sprite
	{
		private var _state:String;
		
		public function EndGameFX(state:String)
		{
			_state = state;
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			var bg:Quad = new Quad(1080,1920,0x000000);
			this.addChild(bg);
			bg.x = 0;
			bg.y = 0;
			bg.alpha = 0.75;
			

			var img:Image = new Image(Assets.assetsManager.getTexture(_state));
			this.addChild(img);
			img.pivotX = img.width * 0.5;
			img.pivotY = img.height * 0.5;
			
			switch(_state)
			{
				case EndGameState.WIN:
					img.x = bg.width * 0.5;
					img.y = bg.height * 0.5; 
					img.scale = 0;
					TweenLite.to(img,0.75,{scale:1, ease:Elastic.easeOut,onComplete:onTweenDone});
					break;
				case EndGameState.TIME_UP:
					img.x = bg.width * 0.5;
					img.y = bg.height * 0.5 + 100; 
					img.alpha = 0;
					TweenLite.to(img,0.75,{y:bg.height * 0.5, alpha:1, ease:Back.easeOut,onComplete:onTweenDone});
					break;
				case EndGameState.OUT_OF_MOVES:
					img.x = -500;
					img.y = bg.height * 0.5;
					TweenLite.to(img,0.75,{x:bg.width * 0.5, ease:Elastic.easeOut,onComplete:onTweenDone});
					break;
			}
			function onTweenDone():void	
			{
				switch(_state)
				{
					case EndGameState.WIN:
						TweenLite.to(img,0.75,{delay:2.5, scale:0, ease:Elastic.easeIn,onComplete:onFXDone});
						break;
					case EndGameState.TIME_UP:
						TweenLite.to(img,0.75,{delay:2.5, alpha:0,onComplete:onFXDone});
						break;
					case EndGameState.OUT_OF_MOVES:
						TweenLite.to(img,0.75,{delay:2.5,x:1500, ease:Back.easeIn,onComplete:onFXDone});
						break;
				}
			}
			function onFXDone():void	
			{
				
				GameManager.gamePlayView.showGameOverScrn();
//				setTimeout(GameManager.gamePlayView.showGameOverScrn,);
			}
		}
	}
}