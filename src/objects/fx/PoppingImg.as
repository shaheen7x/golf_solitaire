package objects.fx
{
	import com.greensock.TweenLite;
	import com.greensock.easing.BounceInOut;
	
	import assets.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class PoppingImg extends Sprite
	{
		private var _value:String;
		public function PoppingImg(value:String)
		{
			_value = value;
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var img:Image = new Image(Assets.assetsManager.getTexture(_value));
			this.addChild(img);
		}
	}
}