package objects.fx
{
	import assets.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Points extends Sprite
	{
		private var  _value:int;
		public function Points(value:int)
		{
			_value = value;
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var img:Image = new Image(Assets.assetsManager.getTexture("points_"+_value));
			this.addChild(img);
		}
	}
}