package objects
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Circ;
	import com.greensock.easing.Ease;
	import com.greensock.easing.Quint;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.setTimeout;
	
	import assets.Assets;
	
	import data.Settings;
	
	import managers.GameManager;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.display.Sprite3D;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import utils.MathHelper;
	
	public class PlayingCard extends Sprite
	{
		public static const DIAMONDS:String = "d";
		public static const HEARTS:String = "h";
		public static const CLUBS:String = "c";
		public static const SPADES:String = "s";
		public static const KING:String = "_k";
		public static const QUEEN:String = "_q";
		public static const JACK:String = "_j";
		public static const _10:String = "10";
		public static const _9:String = "09";
		public static const _8:String = "08";
		public static const _7:String = "07";
		public static const _6:String = "06";
		public static const _5:String = "05";
		public static const _4:String = "04";
		public static const _3:String = "03";
		public static const _2:String = "02";
		public static const ACE:String = "01";
		
		public static const TABLEAU:String = "TABLEAU";
		public static const FOUNDATION:String = "FOUNDATION";
		public static const STOCK:String = "STOCK";
		public static const SPLIT:String = "SPLIT";
		
		public static const SUITS_ARRAY:Array = [CLUBS,DIAMONDS,HEARTS,SPADES];
		public static const RANKS_ARRAY:Array = [ACE,_2,_3,_4,_5,_6,_7,_8,_9,_10,JACK,QUEEN,KING];
		
		private var _cardName:String;
		private var _value:int;
		private var _suit:String;
		private var _rank:String;
		private var _isFaceUp:Boolean;
		private var _playCardType:String;
		public var card:PlayingCardFace;
		private var highLight:Image;
		
		private var soundChannel:SoundChannel;
		private var soundTransform:SoundTransform;
		
		public function PlayingCard(suit:String, rank:String)
		{
			super();
			_suit = suit;
			_rank = rank;
			_cardName = _suit+_rank;
			
			switch(rank)
			{
				case KING:
					_value = 13;
					break;
				case QUEEN:
					_value = 12;
					break;
				case JACK:
					_value = 11;
					break;
				case _10:
					_value = 10;
					break;
				case _9:
					_value = 9;
					break;
				case _8:
					_value = 8;
					break;
				case _7:
					_value = 7;
					break;
				case _6:
					_value = 6;
					break;
				case _5:
					_value = 5;
					break;
				case _4:
					_value = 4;
					break;
				case _3:
					_value = 3;
					break;
				case _2:
					_value = 2;
					break;
				case ACE:
					_value = 1;
					break;				
			}
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			draw();
		}
		public function draw():void
		{
			soundTransform = new SoundTransform(0.25); 
			
			card = new PlayingCardFace(cardName);
			this.addChild(card);
			card.width = this.width;
			card.height = this.height
			card.pivotX = card.width/2;
			card.pivotY = card.height/2;
//			trace("card.width "+card.width);
//			trace("card.height "+card.height);
			
			card.x = this.width/2;
			card.y = this.height/2;
			_isFaceUp = card.isFaceUp;
			this.addEventListener(TouchEvent.TOUCH, onTapped);
			
			highLight = new Image(Assets.assetsManager.getTexture("card_highlight"));
			this.addChild(highLight);
			highLight.width = this.width;
			highLight.height = this.height;
			highLight.x = 0;
			highLight.y = 0;
			highLight.alpha = 0;
		}
		private function onTapped(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch != null)
			{				
				if(touch.phase == TouchPhase.BEGAN)
				{
//					flipCard();
//					this.removeEventListener(TouchEvent.TOUCH, onTapped);
					switch(playCardType)
					{
						case TABLEAU:
							if(GameManager.gamePlayView.checkIsMovable(this))
							{
								stopHighlightingCard();
							}
							break;
						case FOUNDATION:
							GameManager.gamePlayView.moveToSplit(this);
							stopHighlightingCard();
							break;
						case STOCK:
							GameManager.gamePlayView.moveToFoundation(this);
							stopHighlightingCard();
							break;
						case SPLIT:
							GameManager.gamePlayView.moveToFoundation(this);
							stopHighlightingCard();
							break;
						
					}
				}
			}			
		}
		
		public function flipCard(duration:Number = 0.25):void
		{
			soundChannel = Assets.assetsManager.playSound("flipcard",0,0,soundTransform);
			TweenLite.to(card , duration ,{delay:0.00, rotationY:MathHelper.degreesToRadians(180), ease:Quint.easeInOut, onComplete:onFlippingDone});
//			TweenLite.to(card , 0.25 ,{rotationY:MathHelper.degreesToRadians(180), ease:Quint.easeInOut, onComplete:onFlippingDone});
			
		}		
		
		private function onFlippingDone():void
		{
			
			card.rotationY = 0;
//			card.x = 0;
			
			if(card.isFaceUp)
			{
				card.setFaceDown();				
			}else{
				card.setFaceUp();
			}
			_isFaceUp = card.isFaceUp;
			this.addEventListener(TouchEvent.TOUCH, onTapped);
//			trace("_isFaceUp",_isFaceUp)
		}
		public function flipCardAndAddOnTop(duration:Number = 0.25):void
		{
			soundChannel = Assets.assetsManager.playSound("flipcard",0,0,soundTransform);
			TweenLite.to(card , duration,{delay:0.0, rotationY:MathHelper.degreesToRadians(180), ease:Quint.easeInOut, onComplete:onFlippingDoneAndAddOnTop});
			//			TweenLite.to(card , 0.25 ,{rotationY:MathHelper.degreesToRadians(180), ease:Quint.easeInOut, onComplete:onFlippingDone});
			this.parent.addChild(this);
		}
		private function onFlippingDoneAndAddOnTop():void
		{
			
			card.rotationY = 0;
//			card.x = 0;
			
			if(card.isFaceUp)
			{
				card.setFaceDown();				
			}else{
				card.setFaceUp();
			}
			_isFaceUp = card.isFaceUp;
//			this.parent.addChild(this);
			this.addEventListener(TouchEvent.TOUCH, onTapped);
			//			trace("_isFaceUp",_isFaceUp)
		}
		public function addOnTop():void
		{
			this.parent.addChild(this);
		}
		public function highLightCard():void	
		{
			var counter:int = 0;
			fadeIn();
//			var card_y:Number = this.y;
			function fadeIn():void
			{
				if(counter == 2)
				{
					GameManager.gamePlayView.hintButton.enabled = true;
					return;
				}
				TweenLite.to(highLight,0.25,{alpha:1,ease:Circ.easeInOut, onComplete:fadeOut});
//				TweenLite.to(this,0.25,{y:card_y - 20,ease:Circ.easeInOut});
			}
			function fadeOut():void
			{
				counter++;
				TweenLite.to(highLight,0.25,{alpha:0,ease:Circ.easeInOut, onComplete:fadeIn});
//				TweenLite.to(this,0.25,{y:card_y,ease:Circ.easeInOut});
			}
		}
		private function stopHighlightingCard():void
		{
			TweenLite.killTweensOf(highLight);
			highLight.alpha = 0;
			GameManager.gamePlayView.hintButton.enabled = true;
		}
		
		public function get cardName():String
		{
			return _cardName;
		}

		public function get value():int
		{
			return _value;
		}

		public function get suit():String
		{
			return _suit;
		}

		public function get rank():String
		{
			return _rank;
		}

		public function get isFaceUp():Boolean
		{
			return _isFaceUp;
		}

		public function get playCardType():String
		{
			return _playCardType;
		}

		public function set playCardType(value:String):void
		{
			_playCardType = value;
		}



	}
}