package objects
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	
	import assets.Assets;
	
	import data.Settings;
	
	import managers.GameManager;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.utils.Align;
	
	public class GameOverPanel extends Sprite
	{
		private var playBtn:Button;
		public function GameOverPanel()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{	
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var background:Quad = new Quad(1080,1920,0x000000);
			background.alpha = 0.92;
			this.addChild(background);
			
			var scoreBox:Image = new Image(Assets.assetsManager.getTexture("box"));
			this.addChild(scoreBox);
			scoreBox.pivotX = scoreBox.width * 0.5;
			scoreBox.pivotY = scoreBox.height * 0.5;
			scoreBox.x = 1080 * 0.5;
			scoreBox.y = 1920 * 0.5;
			scoreBox.scale = 0.9;
			TweenLite.to(scoreBox,0.5,{scale:1,ease:Back.easeOut});
			
			var boxLabel:TextField = new TextField(905,120,"0");
			boxLabel.format = new TextFormat("BarlowCondensedBlackItalic",120,0xffffff,Align.CENTER,Align.CENTER);			
			boxLabel.text = "GAME OVER" ;			
			this.addChild(boxLabel);
			boxLabel.pivotX = boxLabel.width * 0.5;
			boxLabel.pivotY = boxLabel.height * 0.5;
			boxLabel.x = scoreBox.x;
			boxLabel.y = scoreBox.y - scoreBox.height*0.5 + 80;
			boxLabel.scale = 0.9;
			TweenLite.to(boxLabel,0.5,{scale:1,ease:Back.easeOut});
			
			var delay:Number = 0.25;
			
			var scoreLabel:TextField = new TextField(400,100,"0");
			scoreLabel.format = new TextFormat("BarlowCondensedMediumItalic",90,0xffffff,Align.RIGHT,Align.CENTER);			
			scoreLabel.text = "Score" ;			
			this.addChild(scoreLabel);
			scoreLabel.x = 70;
			scoreLabel.y = boxLabel.y + boxLabel.height + 30;
			scoreLabel.alpha = 0;
			TweenLite.to(scoreLabel,0.5,{delay:0.75+1*delay, x:scoreLabel.x+100,alpha:1,ease:Back.easeOut});
			
			var scoreLabelValue:TextField = new TextField(250,100,"0");
			scoreLabelValue.format = new TextFormat("BarlowCondensedBlackItalic",90,0xffffff,Align.RIGHT,Align.CENTER);			
			scoreLabelValue.text = Settings.gameScore.toString();			
			this.addChild(scoreLabelValue);
			scoreLabelValue.x = scoreLabel.x+scoreLabel.width + 20;
			scoreLabelValue.y = scoreLabel.y;
			scoreLabelValue.alpha = 0;
			TweenLite.to(scoreLabelValue,0.5,{delay:0.75+2*delay, x:scoreLabelValue.x+100,alpha:1,ease:Back.easeOut});
			
			var timeBonusLabel:TextField = new TextField(400,100,"0");
			timeBonusLabel.format = new TextFormat("BarlowCondensedMediumItalic",90,0xffffff,Align.RIGHT,Align.CENTER);			
			timeBonusLabel.text = "Time bonus";			
			this.addChild(timeBonusLabel);
			timeBonusLabel.x = 70;
			timeBonusLabel.y = scoreLabel.y + scoreLabel.height + 30;
			timeBonusLabel.alpha = 0;
			TweenLite.to(timeBonusLabel,0.5,{delay:0.75+3*delay, x:timeBonusLabel.x+100,alpha:1,ease:Back.easeOut});
			
			var timeBonusLabelValue:TextField = new TextField(250,100,"0");
			timeBonusLabelValue.format = new TextFormat("BarlowCondensedBlackItalic",90,0xffffff,Align.RIGHT,Align.CENTER);			
			timeBonusLabelValue.text = Settings.timeBonus.toString() ;			
			this.addChild(timeBonusLabelValue);
			timeBonusLabelValue.x = timeBonusLabel.x+timeBonusLabel.width + 20;
			timeBonusLabelValue.y = timeBonusLabel.y;
			timeBonusLabelValue.alpha = 0;
			TweenLite.to(timeBonusLabelValue,0.5,{delay:0.75+4*delay, x:timeBonusLabelValue.x+100,alpha:1,ease:Back.easeOut});
			
			var line:Quad = new Quad(700,2,0xffffff);
			this.addChild(line);
			line.x = (1080 - 700)*0.5;
			line.y = timeBonusLabel.y + timeBonusLabel.height + 20;
			line.scaleX = 0;
			line.alpha = 0;
			TweenLite.to(line,0.5,{delay:0.75+5*delay, scaleX:1,alpha:1});
			
			
			
			var finalScoreLabel:TextField = new TextField(400,100,"0");
			finalScoreLabel.format = new TextFormat("BarlowCondensedMediumItalic",90,0xffffff,Align.RIGHT,Align.CENTER);			
			finalScoreLabel.text = "Final score";			
			this.addChild(finalScoreLabel);
			finalScoreLabel.x = 70;
			finalScoreLabel.y = timeBonusLabel.y + timeBonusLabel.height + 50;
			finalScoreLabel.alpha = 0;
			TweenLite.to(finalScoreLabel,0.5,{delay:0.75+6*delay, x:finalScoreLabel.x+100,alpha:1,ease:Back.easeOut});
			
			var finalScoreLabelValue:TextField = new TextField(250,100,"0");
			finalScoreLabelValue.format = new TextFormat("BarlowCondensedBlackItalic",90,0xffffff,Align.RIGHT,Align.CENTER);			
			var finalScore:int = Settings.timeBonus + Settings.gameScore;
			finalScoreLabelValue.text = finalScore.toString();			
			this.addChild(finalScoreLabelValue);
			finalScoreLabelValue.x = finalScoreLabel.x+finalScoreLabel.width + 20;
			finalScoreLabelValue.y = finalScoreLabel.y;
			finalScoreLabelValue.alpha = 0;
			TweenLite.to(finalScoreLabelValue,0.5,{delay:0.75+7*delay, x:finalScoreLabelValue.x+100,alpha:1,ease:Back.easeOut});
			
			
//			var whiteBox:Quad = new Quad(1080,207,0xffffff);
//			this.addChild(whiteBox);
//			whiteBox.x = 0;
//			whiteBox.y = 1920 * 0.75;
			
			playBtn = new Button(Assets.assetsManager.getTexture("play_button"));
//			playBtn = new Button(null);
//			playBtn.text = "PLAY";
//			playBtn.textFormat = new TextFormat("BarlowCondensedBlackItalic",120,0xffffff,Align.CENTER,Align.CENTER);
			playBtn.addEventListener(Event.TRIGGERED, onPlayBtnTriggered);		
			this.addChild(playBtn);
//			playBtn.width = Starling.current.stage.stageWidth * 0.5;
//			playBtn.height = playBtn.width * 0.3;
			playBtn.x = background.width *0.5 - playBtn.width * 0.5;
			playBtn.y = finalScoreLabel.y + finalScoreLabel.height + 120;
			playBtn.alpha = 0;
			TweenLite.to(playBtn,0.5,{delay:0.75+6*delay, y:playBtn.y - 50,alpha:1,ease:Back.easeOut});
			
			
			
			
		}
		
		private function onPlayBtnTriggered(e:Event):void
		{
			this.removeFromParent(true);
			GameManager.gamePlayView.reset();
		}
	}
}