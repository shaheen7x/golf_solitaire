package core
{
	import assets.Assets;
	
	import data.Settings;
	
	import events.GameEvents;
	
	import objects.GameContainer;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import views.GamePlayView;
	import views.StartView;
	
	public class MainStarling extends Sprite
	{
		private var gameContainer:GameContainer;
		private var iphoneImg:Image;
		private var startView:StartView;
		private var gamePlayView:GamePlayView;
		private var isReplay:Boolean = false;
		private var progressBar:Quad;
		private var progressBarBG:Quad;
			
		public function MainStarling()
		{
			super();
			var background:Quad = new Quad(Starling.current.nativeStage.fullScreenWidth,Starling.current.nativeStage.fullScreenHeight,0x000000);
			this.addChild(background);
			
			progressBarBG = new Quad(background.width * 0.75, 10, 0x770000);
			this.addChild(progressBarBG);
			progressBarBG.x = background.width * 0.125;
			progressBarBG.y = background.height* 0.75;
			
			progressBar = new Quad(1, 10, 0xffffff);
			this.addChild(progressBar);
			progressBar.x = progressBarBG.x;
			progressBar.y = progressBarBG.y;
//			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
//			
//		}
//		private function onAddedToStage(e:starling.events.Event):void
//		{
//			this.removeEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			trace("MainApp added to Stage");
			
			Assets.init(this);
		}
		
		public function updateProgressBar(value:Number):void
		{
			progressBar.width = progressBarBG.width * value;
		}
		public function StartApp():void
		{
			gameContainer = new GameContainer;

			this.addChild(gameContainer);
			gameContainer.x = 0;
			gameContainer.y = 0;
			gameContainer.width = Starling.current.stage.stageWidth;
			gameContainer.height = Starling.current.stage.stageHeight;
			trace("stage.stageWidth",Starling.current.stage.stageWidth,"stage.stageHeight",Starling.current.stage.stageHeight);

			
			
			startView = new StartView;
			startView.addEventListener(GameEvents.PLAY, onPlayBtnTriggered);
			gameContainer.addChild(startView);
			
			Settings.selectedDeckBack = "back_red";

			
		}
		
		private function onPlayBtnTriggered():void
		{

			gamePlayView = new GamePlayView;
			gameContainer.addChild(gamePlayView);			
		}

	}
}