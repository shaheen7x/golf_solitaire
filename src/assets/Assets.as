package assets
{
	import flash.filesystem.File;
	import flash.text.Font;
	
	import core.MainStarling;
	
	import starling.assets.AssetManager;

	public class Assets
	{
		public static var assetsManager:starling.assets.AssetManager = new AssetManager;
		private static var mainApp:MainStarling;
		
		[Embed(source="fonts/Bangers.ttf",fontFamily="bangers",mimeType="application/x-font",embedAsCFF="false")]
		protected static const bangers:Class;
		
		[Embed(source="fonts/BarlowCondensed-BlackItalic.ttf",fontFamily="BarlowCondensedBlackItalic",mimeType="application/x-font",embedAsCFF="false")]
		protected static const BarlowCondensedBlackItalic:Class;
		
		[Embed(source="fonts/BarlowCondensed-MediumItalic.ttf",fontFamily="BarlowCondensedMediumItalic",mimeType="application/x-font",embedAsCFF="false")]
		protected static const BarlowCondensedMediumItalic:Class;
		
		public static function init(_mainApp:MainStarling):void
		{
			Font.registerFont(bangers);
			Font.registerFont(BarlowCondensedBlackItalic);
			Font.registerFont(BarlowCondensedMediumItalic);
			mainApp = _mainApp;
			var appDir:File = File.applicationDirectory;
//			trace(appDir.url);
//			trace(appDir.nativePath);
			assetsManager.enqueue(appDir.resolvePath("assets"));	
			assetsManager.loadQueue(mainApp.StartApp,null,QueueLoading);
		}
		private static function QueueLoading(ratio:Number):void
		{
			mainApp.updateProgressBar(ratio);
//				trace("Loading assets, progress:", ratio);				
// when the ratio equals '1', we are finished.
//			if (ratio == 1.0)
//			mainApp.StartApp();
		}
	}
}