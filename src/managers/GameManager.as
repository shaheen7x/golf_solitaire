package managers
{
	import views.GamePlayView;

	public class GameManager
	{
		static private var _gamePlayView:GamePlayView;
		public function GameManager()
		{
		}

		public static function get gamePlayView():GamePlayView
		{
			return _gamePlayView;
		}

		public static function set gamePlayView(value:GamePlayView):void
		{
			_gamePlayView = value;
		}

	}
}