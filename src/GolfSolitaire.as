package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import core.MainStarling;
	
	import starling.core.Starling;
	
	[SWF(frameRate="30", backgroundColor="#ffffff")]
	
	public class GolfSolitaire extends Sprite
	{
		private var _starling:Starling;
		
		public function GolfSolitaire()
		{
			super();
			
			// support autoOrients
//			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
//			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			trace("STARLING v."+Starling.VERSION);
			var viewPort:Rectangle = new Rectangle(0,0,stage.fullScreenWidth,stage.fullScreenHeight);
			trace("stage.fullScreenWidth",stage.fullScreenWidth,"stage.fullScreenHeight",stage.fullScreenHeight);
			_starling = new Starling(MainStarling, stage, viewPort);
			_starling.antiAliasing = 16;
			_starling.start();
			
		}
		
		protected function onRemovedFromStage(event:Event):void
		{
			trace("remove game");
			_starling.stop();
			_starling.dispose();
		}
	}
}